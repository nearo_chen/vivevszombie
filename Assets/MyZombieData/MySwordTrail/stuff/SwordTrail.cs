﻿using UnityEngine;

namespace MySwordTrail
{
    /// <summary>
    /// http://blog.csdn.net/akof1314/article/details/37603559
    /// </summary>
    public class SwordTrail : MonoBehaviour
    {
        MySwordTrail.WeaponTrail _weaponTrail;

        private float t = 0.033f;
        private float tempT = 0;
        private float animationIncrement = 0.003f;

        // Use this for initialization
        void Start()
        {
            _weaponTrail = GetComponent<WeaponTrail>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        void LateUpdate()
        {
            t = Mathf.Clamp(Time.deltaTime, 0, 0.066f);

            if (t > 0)
            {
                while (tempT < t)
                {
                    tempT += animationIncrement;

                    if (_weaponTrail.time > 0)
                    {
                        _weaponTrail.Itterate(Time.time - t + tempT);
                    }
                    else
                    {
                        _weaponTrail.ClearTrail();
                    }
                }

                tempT -= t;

                if (_weaponTrail.time > 0)
                {
                    _weaponTrail.UpdateTrail(Time.time, t);
                }
            }
        }
    }
}