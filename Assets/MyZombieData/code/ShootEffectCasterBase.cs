﻿using UnityEngine;
using System.Collections;

public abstract class ShootEffectCasterBase : MonoBehaviour
{
    public AudioSource GunFireSound;

    private float _cooldown;
    private bool _isFire = false;

    public bool SingleShoot = false;
    private bool _isTriggering = false;

    public float CooldownSec;

    // Use this for initialization
    void Awake()
    {
    }

    virtual protected void Update()
    {
        if (_cooldown > 0) { _cooldown -= Time.deltaTime; }

        if (!_isFire)
            return;

        if (_isFire && _cooldown <= 0)
        {
            PlayGunFireSound();
            
            OnFire();

            _cooldown += CooldownSec;
        }

        if (_isFire && SingleShoot)
            _isFire = false;
    }

    abstract protected void OnFire();
    abstract protected void OnStopFire();
    virtual protected void PlayGunFireSound()
    {
        if (GunFireSound != null)
            GunFireSound.PlayOneShot(GunFireSound.clip);
    }

    public void Fire(bool Isfire)
    {
        if (Isfire)
        {
            if (SingleShoot && _isTriggering)
                return;
            _isFire = true;
            _isTriggering = true;
        }
        else
        {
            _isTriggering = false;
            _isFire = false;

            OnStopFire();
        }

      //  Debug.Log("_isFire : " + _isFire);

        //if (!Isfire)
        //    _singleShoot = false;

        //if (_singleShoot)
        //    return;
        //_isFire = Isfire;

        //_singleShoot = SingleShoot;
    }
}
