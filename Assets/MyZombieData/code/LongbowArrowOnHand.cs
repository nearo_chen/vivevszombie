﻿using UnityEngine;
using System.Collections;
using System;

public class LongbowArrowOnHand : ShootEffectCasterBase
{
    public Transform LongbowSpringPos;
    public Transform LongbowShootOutPos;

    public LongbowEffectCaster longbowEffectCaster;
    public GameObject ArrowOnHandModel;

    public float SpringPullMaxDis = 0.2f;

#if UNITY_EDITOR
    [Tooltip("For debug manual unfire")]
    public bool DebugManualUnfire;
#endif

    enum ArrowState
    {
        OnHand,
        OnSpring,
        GettingArrow,
    }
    ArrowState _nowState = ArrowState.OnHand;

    public float GettingArrowSec;
    float _gettingArrowSecCount;

    protected override void OnFire()
    {
        if (_nowState == ArrowState.OnHand)
        {
            float _longbowSpringRadius = LongbowSpringPos.localScale.x * 0.5f;
            Vector3 offset = transform.position - LongbowSpringPos.position;
            if (offset.sqrMagnitude <= _longbowSpringRadius * _longbowSpringRadius)
            {
                ArrowOnHandModel.SetActive(false);
                longbowEffectCaster.SetArrowOnSpring();
                _nowState = ArrowState.OnSpring;
            }
        }
    }

    protected override void OnStopFire()
    {
#if UNITY_EDITOR
        //Debug.Log("OnStopFire()");
        if (DebugManualUnfire)
            return;
#endif

        Debug.Log("[OnStopFire] _nowState : " + _nowState.ToString());
        if (_nowState == ArrowState.OnSpring)
        {
            longbowEffectCaster.SetArrowShootOut(_pullRatio, _shootOutDir);
            _nowState = ArrowState.GettingArrow;
        }
    }

    float _pullRatio;
    Vector3 _shootOutDir;

    protected override void Update()
    {
        base.Update();

        if (_nowState == ArrowState.GettingArrow)
        {
            if (_gettingArrowSecCount < GettingArrowSec)
            {
                _gettingArrowSecCount += Time.deltaTime;
                return;
            }
            _gettingArrowSecCount = 0;
            _nowState = ArrowState.OnHand;
            ArrowOnHandModel.SetActive(true);
        }
        else if (_nowState == ArrowState.OnSpring)
        {
            _shootOutDir = (LongbowShootOutPos.position - LongbowSpringPos.position).normalized;
            Plane plane = new Plane(-_shootOutDir, LongbowSpringPos.position);
            float dis = plane.GetDistanceToPoint(transform.position);
            //Debug.Log("dis : " + dis);
            //Debug.DrawLine(LongbowSpringPos.position, LongbowSpringPos.position + plane.normal * dis);
            //MyHelpDraw.LineDraw(LongbowSpringPos, LongbowSpringPos.position + plane.normal * dis, 0.05f, 0.05f, Color.white);

            _pullRatio = Mathf.Clamp01(dis / SpringPullMaxDis);
            longbowEffectCaster.SetSpringPullRatio(_pullRatio);
        }
    }


}
