﻿using UnityEngine;
using System.Collections;

public class ZombieSounds : MonoBehaviour
{
	public AudioClip [] ZombieIdle;
    public AudioClip[] ZombieHurt;
    public AudioClip[] ZombieDead;

    public AudioClip[] ZombieAttack;

    enum ZombieSoundState
    {
        Idle,
        Hurt,
        Attack,
        Dead,
    }
    ZombieSoundState currentState = ZombieSoundState.Idle;
    AudioSource mSource;

	// Use this for initialization
	void Start () {
        //currentState = ZombieSoundState.Idle;
        mSource = GetComponent<AudioSource> ();
		mSource.clip = ZombieIdle[Random.Range (0, ZombieIdle.Length)];
		mSource.Play ();
	}

    public void InitState()
    {
        currentState = ZombieSoundState.Idle;
    }

    // Update is called once per frame
    void Update ()
	{
        if (currentState == ZombieSoundState.Hurt && !mSource.isPlaying)
            currentState = ZombieSoundState.Idle;

        //if (currentState == ZombieSoundState.Attack && !mSource.isPlaying)
        //    currentState = ZombieSoundState.Idle;

        if (!mSource.isPlaying && currentState == ZombieSoundState.Idle)
		{
            if (currentState == ZombieSoundState.Idle)
            {
                AudioClip clip = ZombieIdle[Random.Range(0, ZombieIdle.Length)];
                mSource.clip = clip;
                mSource.Play();
                //Debug.Log("idle Play : " + clip.name);
                
                
            }
		}
	}

    public void hurt()
    {
        if (currentState == ZombieSoundState.Hurt && mSource.isPlaying)
            return;

        currentState = ZombieSoundState.Hurt;
        AudioClip clip = ZombieHurt[Random.Range(0, ZombieHurt.Length)];     
        mSource.PlayOneShot(clip, 1.0f);
        //Debug.Log("hurt PlayOneShot : " + clip.name);
    }

    public void dead()
    {
        currentState = ZombieSoundState.Dead;
       
        AudioClip clip = ZombieDead[Random.Range(0, ZombieDead.Length)];
      //  mSource.Stop();
        mSource.PlayOneShot(clip, 1.0f);
        //Debug.Log("dead PlayOneShot : " + clip.name);
    }

    public void attack()
    {
       // if (currentState == ZombieSoundState.Attack && mSource.isPlaying)
       //     return;

        currentState = ZombieSoundState.Attack;
        AudioClip clip = ZombieAttack[Random.Range(0, ZombieAttack.Length)];
       // mSource.Stop();
        mSource.PlayOneShot(clip, 1.0f);
//Debug.Log("attack PlayOneShot : " + clip.name);
    }

    //public void EnableHurtState()
    //{
    //  //     currentState = (enable) ? ZombieSoundState.Hurt : ZombieSoundState.Idle;
    //}

    //public void EnableAttackState()
    //{
    //    currentState = ZombieSoundState.Attack;
    //   // currentState = (enable)?ZombieSoundState.Attack : ZombieSoundState.Idle;
    //}
}
