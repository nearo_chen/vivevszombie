﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;

/// <summary>
/// Triggering Hit Reactions on mouse button.
/// </summary>
public class MyHitReactionTrigger : MonoBehaviour
{   
    public RootMotion.FinalIK.HitReaction hitReaction;
  
    float hitForce = 1f;

    public HPManager mHPManager;
    public GameObject ZombiePrefeb;
    private string colliderName;

    //private bool mIsTrigger = false;
    //private Ray mGunTriggerRay;
    //public void SetGunFireRay(Ray ray)
    //{
    //    mIsTrigger = true;
    //    mGunTriggerRay = ray;
    //}

    bool mIsHitted;
    RaycastHit mRayHitted;
    Ray mHitRay;
    public void SetRaycastHit(RaycastHit hit, Ray hitRay)
    {
       // Debug.Log("SetRaycastHit");
        mIsHitted = true;
        mRayHitted = hit;
        mHitRay = hitRay;
    }

    public bool MouseHit = true;
    void Update()
    {
        //bool isTrigger = false;
        //RaycastHit hit;

        //if(mIsTrigger)
        //{
        //    mIsTrigger = false;

        //    hit = new RaycastHit();
        //    isTrigger = true;
        //}

        if (MouseHit)
        {
            // On left mouse button...
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                // Raycast to find a ragdoll collider
                RaycastHit hit = new RaycastHit();
                if (Physics.Raycast(ray, out hit, 100f))
                {
                    Transform zombie = null;
                    MyHelpNode.FindParentTag(hit.collider.transform, ZombiePrefeb.tag, ref zombie);
                    if (zombie != null)
                        SetRaycastHit(hit, ray);
                }
            }
        }

       
        if (mIsHitted)
        {
            mIsHitted = false;
            mHPManager.SetHitName(colliderName, mRayHitted.point, mRayHitted.transform, mHitRay);

            BodyCrackEffect.BodyCrackInfo crackInfo = mHPManager.GetBodyCrackInfo(mRayHitted.collider.transform);

            Collider realBoneCollider = mRayHitted.collider;
            if (crackInfo != null && crackInfo.RealBone != null) {
                realBoneCollider = crackInfo.RealBone.GetComponent<Collider>();
            }

            // Use the HitReaction
            //hitReaction.Hit(mRayHitted.collider, mHitRay.direction * hitForce, mRayHitted.point);
            hitReaction.Hit(realBoneCollider, mHitRay.direction * hitForce, mRayHitted.point);

            // Just for GUI
            colliderName = mRayHitted.collider.name;

            
        }
    }

    void OnGUI()
    {
        //	GUILayout.Label("LMB to shoot the Dummy, RMB to rotate the camera.");
        if (colliderName != string.Empty)
            GUILayout.Label(

"\n                                                                  " +
"Last Bone Hit: " + colliderName);
    }


    
    
}

