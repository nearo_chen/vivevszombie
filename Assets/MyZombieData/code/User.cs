﻿using UnityEngine;
using System.Collections;

public class User : MonoBehaviour {

    public float HP=100;
    public float SplatOnCameraDisMax = 2.0f;
    public float SplatOnCameraDisMin = 1.0f;
    public Camera camera;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        pos.y += 0.2f;
        Debug.DrawLine(pos, pos + transform.forward * SplatOnCameraDisMax, Color.white);

        pos.y += 0.2f;
        Debug.DrawLine(pos, pos + transform.forward * SplatOnCameraDisMin, Color.red);
    }

    public void Splat_Blood_OnScreen(float sqrMagnitude)
    {
        Debug.Log("triger ZombieCrack() sqrMagnitude:" + sqrMagnitude);

        //dis max: 2(camera blood 1 time) min:1(camera blood 3 time)
        if (sqrMagnitude < SplatOnCameraDisMax * SplatOnCameraDisMax)
            camera.gameObject.GetComponent<splatControl_red>().makeSplat();

        float middle = SplatOnCameraDisMin + (SplatOnCameraDisMax - SplatOnCameraDisMin) * 0.5f;
        if (sqrMagnitude < middle * middle)
        {
            camera.gameObject.GetComponent<splatControl_red>().makeSplat();
        }

        if (sqrMagnitude < SplatOnCameraDisMin * SplatOnCameraDisMin)
            camera.gameObject.GetComponent<splatControl_red>().makeSplat();
    }

  public  void AddHP(float addHP)
        {

        //Debug.Log("Add HP :  "+addHP);
        HP += addHP;

        if(addHP<0)
        {
            //screen red
        }

        
       
    }

    void OnGUI()
    {
        //	GUILayout.Label("LMB to shoot the Dummy, RMB to rotate the camera.");
      
            GUILayout.Label("                                                                  " +
"PlayerHP: " + GetComponent<User>().HP);

    }
}
