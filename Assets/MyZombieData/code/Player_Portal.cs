﻿using UnityEngine;
using System.Collections;

public class Player_Portal : MonoBehaviour
{

    public Transform Player;
    public MyHitReactionTrigger hitReactionTrigger;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Splat_Blood_OnScreen(float sqrMagnitude)
    {
        Player.GetComponent<User>().Splat_Blood_OnScreen(sqrMagnitude);
    }

    public void AddHP(float addHP)
    {
        Player.GetComponent<User>().AddHP(addHP);
    }

    public Vector3 GetPlayerPos()
    {
        return Player.position;
    }

    public void HittedZombie(RaycastHit hit, Ray gunRay)
    {
        hitReactionTrigger.SetRaycastHit(hit, gunRay);
    }
}
