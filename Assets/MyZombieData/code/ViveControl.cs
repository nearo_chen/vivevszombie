﻿using UnityEngine;
using System.Collections;
using Valve.VR;
using System.Collections.Generic;

public class ViveControl : MonoBehaviour {

	public SteamVR_TrackedObject _headTrackedObj;
	public SteamVR_TrackedObject _leftTrackedObj;
	public SteamVR_TrackedObject _rightTrackedObj;

	GameObject mLeftGun;
	GameObject mRightGun;

	public List<GameObject> weaponList;
	//bool []mWeaponUse;

	// Use this for initialization
	void Start () {

		for (int a=0; a<weaponList.Count; a++)
			weaponList[a].SetActive(false);

		//mLeftGun = _getWeapon(mLeftGun);
		//mRightGun = _getWeapon(mRightGun);
	}

	static void _processFire(GameObject handWeapon, bool IsFire)
	{
		GunFireMouseTest2 gunFire = handWeapon.GetComponent<GunFireMouseTest2>();
		if (gunFire != null) 
		{
			if(IsFire)
				gunFire.Fire ();
			else
				gunFire.NotFire();
		}
	}

    void returnWeapon(ref GameObject handWeapon)
    {
        if (handWeapon != null)
        {
            handWeapon.SetActive(false);
            weaponList.Add(handWeapon);
            handWeapon = null;
        }
    }

	void _getWeapon(ref GameObject handWeapon)
	{
        returnWeapon(ref handWeapon);

        handWeapon = weaponList[0];
		weaponList.Remove(handWeapon);

        handWeapon.SetActive (true);
		//return get;
	}

	public void Vibrate()
	{
		SteamVR_Controller.Input((int)_leftTrackedObj.index).TriggerHapticPulse();
        SteamVR_Controller.Input((int)_rightTrackedObj.index).TriggerHapticPulse();
    }

	// Update is called once per frame
	void Update () {

		if (_leftTrackedObj.isValid)
		{
            if (mLeftGun == null)
                _getWeapon(ref mLeftGun);

            mLeftGun.transform.position = _leftTrackedObj.transform.position;
			mLeftGun.transform.rotation = _leftTrackedObj.transform.rotation;
			if(
				SteamVR_Controller.Input((int)_leftTrackedObj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Trigger) ||
				SteamVR_Controller.Input((int)_leftTrackedObj.index).GetPressDown(EVRButtonId.k_EButton_ApplicationMenu)
				)
			{
				_processFire(mLeftGun, true);
				Debug.Log("leftGun.Fire();");
			}
            else if (
                SteamVR_Controller.Input((int)_leftTrackedObj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Trigger) ||                
                SteamVR_Controller.Input((int)_leftTrackedObj.index).GetPressUp(EVRButtonId.k_EButton_ApplicationMenu)
                )
            {
				_processFire(mLeftGun, false);
				Debug.Log("leftGun.NotFire();");
            }

			//Switch weapon
			if(
				SteamVR_Controller.Input((int)_leftTrackedObj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Touchpad)
				)
			{
				Debug.Log("k_EButton_SteamVR_Touchpad = GetPressDown _leftTrackedObj");
			}
			else if (
				SteamVR_Controller.Input((int)_leftTrackedObj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Touchpad)
				)
			{
				_getWeapon(ref mLeftGun);
				Debug.Log("k_EButton_SteamVR_Touchpad = GetPressUp _leftTrackedObj");
			}
        }
		else
            returnWeapon(ref mLeftGun);

        if (_rightTrackedObj.isValid)
        {
            if(mRightGun == null)
                _getWeapon(ref mRightGun);
          
            mRightGun.transform.position = _rightTrackedObj.transform.position;
            mRightGun.transform.rotation = _rightTrackedObj.transform.rotation;
            if (
                SteamVR_Controller.Input((int)_rightTrackedObj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Trigger) ||
                SteamVR_Controller.Input((int)_rightTrackedObj.index).GetPressDown(EVRButtonId.k_EButton_ApplicationMenu)
                )

            {
                _processFire(mRightGun, true);
                Debug.Log("rightGun.Fire();");
            }
            else if (
                SteamVR_Controller.Input((int)_rightTrackedObj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Trigger) ||
                SteamVR_Controller.Input((int)_rightTrackedObj.index).GetPressUp(EVRButtonId.k_EButton_ApplicationMenu)
                )

            {
                _processFire(mRightGun, false);
                Debug.Log("rightGun.NotFire();");
            }

            //Switch weapon
            if (
                SteamVR_Controller.Input((int)_rightTrackedObj.index).GetPressDown(EVRButtonId.k_EButton_SteamVR_Touchpad)
                )
            {
                Debug.Log("k_EButton_SteamVR_Touchpad = GetPressDown _rightTrackedObj");
            }
            else if (
                SteamVR_Controller.Input((int)_rightTrackedObj.index).GetPressUp(EVRButtonId.k_EButton_SteamVR_Touchpad)
                )
            {
                _getWeapon(ref mRightGun);
                Debug.Log("k_EButton_SteamVR_Touchpad = GetPressUp _rightTrackedObj");
            }
        }
        else
        {
            returnWeapon(ref mRightGun);
        }
	}
}
