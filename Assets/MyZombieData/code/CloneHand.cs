﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CloneHand : MonoBehaviour
{
    public Transform actor;
    List< GameObject> mCloneObject = new List<GameObject>();
    //public SkinnedMeshRenderer foreArmMesh, handMesh;

    // Use this for initialization
    void Start()
    {

    }

    void OnDestroy()
    {
        foreach(GameObject obj in mCloneObject)
            Destroy(obj);
    }

    public void CloseCollider()
    {
        foreach (GameObject obj in mCloneObject)
            MyHelpNode.CloseColliderRigidBody(obj.transform, 20);
    }

    public GameObject CloneSkinnedMesh(SkinnedMeshRenderer[] skinnedMeshes)
    {
        if (skinnedMeshes == null)
            return null;

        bool allHide = true;
        foreach (SkinnedMeshRenderer skinRenderer in skinnedMeshes)
        {
            if (skinRenderer.enabled)
            {
                allHide = false;
                break;
            }
        }
        if (allHide)
            return null;

        GameObject parent = Clone(skinnedMeshes, true);
        
        parent.AddComponent<Rigidbody>();
        parent.transform.position = actor.position;
        parent.transform.rotation = actor.rotation * Quaternion.Euler(-90, 90, -90);

        mCloneObject.Add(parent);
        return parent;
    }

    private static GameObject Clone(SkinnedMeshRenderer [] skinnedMeshes, bool IsConvex)
    {
        GameObject parent = new GameObject();
        foreach (SkinnedMeshRenderer skinRenderer in skinnedMeshes)
        {
            if (skinRenderer.enabled == false)
                continue;
            GameObject obj = new GameObject();
            obj.transform.parent = parent.transform;
            obj.transform.position = Vector3.zero;
            obj.transform.rotation = Quaternion.identity;
            obj.AddComponent<MeshRenderer>();
            obj.AddComponent<MeshFilter>();
            MeshCollider collider = obj.AddComponent<MeshCollider>();
            Mesh mesh = obj.GetComponent<MeshFilter>().mesh;
            skinRenderer.BakeMesh(mesh);
            obj.GetComponent<MeshRenderer>().material = Instantiate(skinRenderer.material);
            collider.convex = IsConvex;
            collider.sharedMesh = mesh;
        }

        return parent;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyUp(KeyCode.P))
        //{
        //    GameObject parent = new GameObject();

        //    GameObject obj = new GameObject();
        //    obj.transform.parent = parent.transform;
        //    obj.transform.position = Vector3.zero;
        //    obj.transform.rotation = Quaternion.identity;
        //    obj.AddComponent<MeshRenderer>();
        //    obj.AddComponent<MeshFilter>();
        //    MeshCollider collider = obj.AddComponent<MeshCollider>();            
        //    Mesh mesh = obj.GetComponent<MeshFilter>().mesh;
        //    foreArmMesh.BakeMesh(mesh);
        //    obj.GetComponent<MeshRenderer>().material = Instantiate(foreArmMesh.material);
        //    collider.convex = true;
        //    collider.sharedMesh = mesh;


        //    obj = new GameObject();
        //    obj.transform.parent = parent.transform;
        //    obj.transform.position = Vector3.zero;
        //    obj.transform.rotation = Quaternion.identity;
        //    obj.AddComponent<MeshRenderer>();
        //    obj.AddComponent<MeshFilter>();
        //    collider = obj.AddComponent<MeshCollider>();
        //    mesh = obj.GetComponent<MeshFilter>().mesh;
        //    handMesh.BakeMesh(mesh);
        //    obj.GetComponent<MeshRenderer>().material = Instantiate(handMesh.material);
        //    collider.convex = true;
        //    collider.sharedMesh = mesh;

        //    parent.transform.position = actor.position;
        //    parent.transform.rotation = actor.rotation * Quaternion.Euler(-90, 90 , -90);

        //    Rigidbody rigid = parent.AddComponent<Rigidbody>();
        //   // rigid.useGravity = true;
        //   // MeshCollider collider = parent.AddComponent<MeshCollider>();
        //   // collider.sharedMesh = 

        //    //Debug.Log("Input.GetKeyUp(KeyCode.P)");
        //}
    }


}
