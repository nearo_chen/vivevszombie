﻿using System.Collections.Generic;
using UnityEngine;

public class BodyCrackEffect : MonoBehaviour
{
    public bool IsDebug=true;
    public Player_Portal player_Portal;
    public Transform Actor;

    public SkinnedMeshRenderer HeadMesh, LUpperArmMesh, LForeArmMesh, LHandMesh, RUpperArmMesh,
        RForeArmMesh, RHandMesh, LChestMesh, RChestMesh, LWaistMesh, RWaistMesh,
        RThighMesh, LThighMesh, RCalfMesh, LCalfMesh;
    public Transform Head, LUpperArm, LForeArm, RUpperArm, RForeArm, LHand, RHand, LChest, RChest, LWaist, RWaist,
        Pelvis, UpperSpine, RThigh, LThigh, RCalf, LCalf;
    public GameObject BloodFX_HeadExplosion;
    public GameObject BloodFX_HeadSplash;
    public GameObject BloodFX_HeadExplosionSmall;
    public GameObject BloodFX_DropDown;

    public GameObject[] BloodFX_SmallSplash = new GameObject[3];
    public GameObject[] BloodFX_mediumSplash = new GameObject[4];

    float mFallDownSec;
    float? mFallDownTimeCountSec = null;
    RootMotion.FinalIK.RagdollUtility mRagdollUtility;
    Animator mAnimator;
    CloneHand mCloneHand;

    public float FalldownForceMin = 1000f;
    public float FalldownForceMax = 3000f;
    Vector3 mFallDownAddforce;
    Transform mFalldownHitBone;

    public class BodyCrackInfo
    {
        public BodyCrackInfo(SkinnedMeshRenderer crackMeshPart, 
            SkinnedMeshRenderer[] leaveMeshParts, Transform crackBone)
        {
            _init(crackMeshPart, null, leaveMeshParts, crackBone, null);
        }

        public BodyCrackInfo(SkinnedMeshRenderer crackMeshPart, SkinnedMeshRenderer cannot_CrackMeshPart,
            SkinnedMeshRenderer[] leaveMeshParts, Transform crackBone, Transform realParent)
        {
            _init(crackMeshPart, cannot_CrackMeshPart, leaveMeshParts, crackBone, realParent);
        }

        private void _init(SkinnedMeshRenderer crackMeshPart, SkinnedMeshRenderer cannot_CrackMeshPart,
            SkinnedMeshRenderer[] leaveMeshParts, Transform crackBone, Transform realBone)
        {
            Cannot_CrackMeshPart = cannot_CrackMeshPart;
            CrackMeshPart = crackMeshPart;
            LeaveMeshParts = leaveMeshParts;
            CrackBone = crackBone;
            RealBone = realBone;
        }
        public SkinnedMeshRenderer Cannot_CrackMeshPart;
        public SkinnedMeshRenderer CrackMeshPart;
        public SkinnedMeshRenderer[] LeaveMeshParts;
        public Transform CrackBone;
        public Transform RealBone;
    }
    List<BodyCrackInfo> mBodyCrackInfo = new List<BodyCrackInfo>();

    // Use this for initialization
    void Start()
    {
        mCloneHand = GetComponent<CloneHand>();
        mRagdollUtility = Actor.GetComponent<RootMotion.FinalIK.RagdollUtility>();
        mAnimator = Actor.GetComponent<Animator>();

        mBodyCrackInfo.Add(new BodyCrackInfo(
            LHandMesh,
            null,
            LHand));
        mBodyCrackInfo.Add(new BodyCrackInfo(
            RHandMesh,
            null,
            RHand));

        mBodyCrackInfo.Add(new BodyCrackInfo(
            LForeArmMesh,
            new SkinnedMeshRenderer[] { LHandMesh },
            LForeArm));
        mBodyCrackInfo.Add(new BodyCrackInfo(
            RForeArmMesh,
            new SkinnedMeshRenderer[] { RHandMesh },
            RForeArm));

        mBodyCrackInfo.Add(new BodyCrackInfo(
            LUpperArmMesh,
            new SkinnedMeshRenderer[] { LForeArmMesh, LHandMesh },
            LUpperArm));
        mBodyCrackInfo.Add(new BodyCrackInfo(
            RUpperArmMesh,
            new SkinnedMeshRenderer[] { RForeArmMesh, RHandMesh },
            RUpperArm));

        mBodyCrackInfo.Add(new BodyCrackInfo(
            LChestMesh,
            RChestMesh,
            new SkinnedMeshRenderer[] { LUpperArmMesh, LForeArmMesh, LHandMesh },
            LChest,
            UpperSpine));
        mBodyCrackInfo.Add(new BodyCrackInfo(
            RChestMesh,
            LChestMesh,
            new SkinnedMeshRenderer[] { RUpperArmMesh, RForeArmMesh, RHandMesh },
            RChest,
            UpperSpine));

        mBodyCrackInfo.Add(new BodyCrackInfo(
            LWaistMesh,
            RWaistMesh,
            null,
            LWaist,
            Pelvis));
        mBodyCrackInfo.Add(new BodyCrackInfo(
            RWaistMesh,
            LWaistMesh,
            null,
            RWaist,
            Pelvis));

        mBodyCrackInfo.Add(new BodyCrackInfo(
            LCalfMesh,
            null,
            LCalf));
        mBodyCrackInfo.Add(new BodyCrackInfo(
            RCalfMesh,
            null,
            RCalf));

        mBodyCrackInfo.Add(new BodyCrackInfo(
            LThighMesh,
            new SkinnedMeshRenderer[] { LCalfMesh },
            LThigh));
        mBodyCrackInfo.Add(new BodyCrackInfo(
            RThighMesh,
            new SkinnedMeshRenderer[] { RCalfMesh },
            RThigh));

        mColliderDisableNode = new List<Transform>();
        _recColliderDisableNode(Actor, mColliderDisableNode);
    }

    //void LateUpdate()
    //{
    //    if (mFalldownHitBone != null)
    //    {
    //        Debug.Log("AddForce_RigidBody mFalldownHitBone : " + mFalldownHitBone.name + ", force: " + mFallDownAddforce);
    //        mFalldownHitBone.GetComponent<Rigidbody>().AddForce(mFallDownAddforce);
    //        mFalldownHitBone = null;
    //    }
    //}

    bool IsFootAllOK()
    {
        return (RThighMesh.enabled && LThighMesh.enabled && RCalfMesh.enabled && LCalfMesh.enabled);
    }

    bool mJustEnableRagdoll = false;
    // Update is called once per frame
    void Update()
    {
        if (mFallDownTimeCountSec.HasValue)
        {
            mFallDownTimeCountSec -= Time.deltaTime;

            if (!IsFootAllOK())
                mFallDownTimeCountSec = 0;

            if (mFallDownTimeCountSec.Value <= 0)
            {
                mFallDownTimeCountSec = null;

                Debug.Log("mRagdollUtility.EnableRagdoll()");
                mRagdollUtility.EnableRagdoll();
                mJustEnableRagdoll = true;
            }
            else
            {
                float speed = Mathf.Clamp(mFallDownTimeCountSec.Value / mFallDownSec, 0.6f, 1.0f);
                mAnimator.speed = speed;
                //Debug.Log("mAnimator.speed :"+ speed);
            }
        }

        foreach (BleedData data in mBleedDataList)
        {
            if (data.objBloodFX_FallDown == null)//particle is dead destroy
            {
                mBleedDataList.Remove(data);
                // Debug.Log("mBleedDataList.Remove(data);");
                break;
            }
            data.setPosition();
        }

        if (mFalldownHitBone != null && mJustEnableRagdoll)
        {
            Debug.Log("JustEnableRagdoll");
            mJustEnableRagdoll = false;
        }
        else if (mFalldownHitBone != null)
        {
            Debug.Log("JustEnableRagdoll next frame");
            Debug.Log("AddForce_RigidBody mFalldownHitBone : " + mFalldownHitBone.name + ", force: " + mFallDownAddforce);
            mFalldownHitBone.GetComponent<Rigidbody>().AddForce(mFallDownAddforce);
            mFalldownHitBone = null;
        }

    }

    public BodyCrackInfo GetBodyCrackInfo(Transform crackBone)
    {
        BodyCrackInfo crackinfo = null;
        foreach (BodyCrackInfo info in mBodyCrackInfo)
        {
            if (info.CrackBone == crackBone)
            {
                crackinfo = info;
                break;
            }
        }
       
        return crackinfo;
    }

    public void Crack_BodyPart(Transform crackBone)
    {
        BodyCrackInfo crackinfo = GetBodyCrackInfo(crackBone);
        if (crackinfo == null)
            Debug.Log("Crack_BodyPart cannot find crackinfo: " + crackBone.name);
        if (crackinfo.Cannot_CrackMeshPart != null && crackinfo.Cannot_CrackMeshPart.enabled == false)
            return;

        //bake leave mesh(before hide leave  mesh)
        mCloneHand.CloneSkinnedMesh(crackinfo.LeaveMeshParts);

        //hide leave mesh
        crackinfo.CrackMeshPart.enabled = false;
        if (crackinfo.LeaveMeshParts != null)
        {
            foreach (SkinnedMeshRenderer skinrenderer in crackinfo.LeaveMeshParts)
            {
                skinrenderer.enabled = false;
                //Debug.Log("disable part: " + skinrenderer.name);
            }
        }

        //disable all child's collider
        _setAllTree_Collider(crackinfo.CrackBone, false);

        //add bleeding particle================================================================
        //BloodFX_SmallSplash---------------------------------------------
        GameObject obj = Instantiate(BloodFX_mediumSplash[Random.Range(0, BloodFX_mediumSplash.Length)]) as GameObject;
        obj.transform.parent = crackinfo.CrackBone;
        
        if (IsDebug)
        {
            var debugSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            debugSphere.transform.parent = crackinfo.CrackBone;
            debugSphere.transform.localRotation = Quaternion.identity;

            debugSphere.transform.localPosition = Vector3.zero;
            CapsuleCollider capsuleColxxx = crackinfo.CrackBone.GetComponent<CapsuleCollider>();
            if (capsuleColxxx != null)
                debugSphere.transform.localPosition = capsuleColxxx.center;
            else
            {
                BoxCollider boxCol = crackinfo.CrackBone.GetComponent<BoxCollider>();
                if (boxCol != null)
                {
                    debugSphere.transform.localPosition = boxCol.center;
                }
            }

            debugSphere.transform.localScale = Vector3.one * 0.1f;
            debugSphere.GetComponent<SphereCollider>().enabled = false;
        }

        CapsuleCollider capsuleCol = crackinfo.CrackBone.GetComponent<CapsuleCollider>();
        if (capsuleCol != null)
        {
            MyHelpNode.SetLocalPosition(obj.transform, capsuleCol.center);
        }
        else
        {
            BoxCollider boxCol = crackinfo.CrackBone.GetComponent<BoxCollider>();
            if (boxCol != null)
            {
                MyHelpNode.SetLocalPosition(obj.transform, boxCol.center);
            }
        }

        //BloodFX_DropDown---------------------------------------------
        obj = Instantiate(BloodFX_DropDown) as GameObject;
        BleedData data = new BleedData();
        data.actorPosRec = Actor.transform.position;
        data.bone = crackinfo.CrackBone;
        data.offset = Vector3.zero;
        data.objBloodFX_FallDown = obj;
        data.setPosition();
        mBleedDataList.Add(data);

        player_Portal.Splat_Blood_OnScreen((Actor.transform.position - player_Portal.Player.position).sqrMagnitude);
    }



    class BleedData
    {
        public Vector3 actorPosRec;
        public Transform bone;
        public Vector3 offset;
        public GameObject objBloodFX_FallDown;

        public void setPosition()
        {
            Transform outObj = null;
            outObj = null;
            _getObjByName(objBloodFX_FallDown.transform, "FallDownBlood", ref outObj);
            outObj.position = bone.TransformPoint(offset);

            outObj = null;
            _getObjByName(objBloodFX_FallDown.transform, "Ripple", ref outObj);
            outObj.position = actorPosRec;

            outObj = null;
            _getObjByName(objBloodFX_FallDown.transform, "FallDown_Plane", ref outObj);
            outObj.position = actorPosRec;
        }
    }
    List<BleedData> mBleedDataList = new List<BleedData>();
    public void Body_hit(Vector3 hitPoint, Transform hitBone) { _body_hit(hitPoint, hitBone, true); }

    public void _body_hit(Vector3 hitPoint, Transform hitBone, bool bleeding)
    {
        //BloodFX_SmallSplash---------------------------------------------
        GameObject obj = Instantiate(BloodFX_SmallSplash[Random.Range(0, BloodFX_SmallSplash.Length)]) as GameObject;
        MyHelpNode.SetLocalPosition(obj.transform, Vector3.zero);
        obj.transform.position = hitPoint;

        //BloodFX_FallDown---------------------------------------------
        if (bleeding)
        {
            obj = Instantiate(BloodFX_DropDown) as GameObject;
            BleedData data = new BleedData();
            data.actorPosRec = Actor.transform.position;
            data.bone = hitBone;
            data.offset = hitBone.InverseTransformPoint(hitPoint);
            data.objBloodFX_FallDown = obj;
            data.setPosition();
            mBleedDataList.Add(data);
        }
    }

    public void Crack_Head()
    {
        // if (_isHeadCrack())
        //    return;

        GameObject obj = null;
        Transform outObj = null;
        //BloodFX_Object------------------------------------------------------
        obj = Instantiate(BloodFX_HeadExplosion) as GameObject;
        outObj = null;
        _getObjByName(obj.transform, "Object", ref outObj);
        outObj.position = Head.transform.position;

        _getObjByName(obj.transform, "SubEmitter_Ground", ref outObj);
        outObj.position = Head.transform.position;

        outObj = null;
        _getObjByName(obj.transform, "SubEmitter_Trail", ref outObj);
        outObj.position = Head.transform.position;

        outObj = null;
        _getObjByName(obj.transform, "Object_Plane", ref outObj);
        outObj.position = Actor.transform.position;
        //new Vector3(Head.transform.position.x, 0, Head.transform.position.z);

        //BloodFX_Splash04------------------------------------------------------
        obj = Instantiate(BloodFX_HeadSplash) as GameObject;
        obj.transform.parent = Head;
        MyHelpNode.SetLocalPosition(obj.transform, Vector3.zero);

        //BloodFX_Explosion------------------------------------------------------
        obj = Instantiate(BloodFX_HeadExplosionSmall) as GameObject;
        MyHelpNode.SetWorldPosition(obj.transform, Head.transform.position);

        outObj = null;
        _getObjByName(obj.transform, "Blood_Explosion_Plane", ref outObj);
        outObj.position = Actor.transform.position;
        //------------------------------------------------------

        _setAllTree_Collider(Head, false);
        HeadMesh.enabled = false;

        player_Portal.Splat_Blood_OnScreen((Actor.transform.position - player_Portal.Player.position).sqrMagnitude);
    }


    public void ActorFallDown(float fallDownTimeSec)
    {
        mFallDownSec = fallDownTimeSec;
        mFallDownTimeCountSec = fallDownTimeSec;
    }

    public void AddForce_RigidBody(Transform hitBone, Ray hitRay)
    {
        mFallDownAddforce = hitRay.direction * Random.Range(FalldownForceMin, FalldownForceMax);
        mFalldownHitBone = hitBone;

    }

    

    List<Transform> mColliderDisableNode;
    static void _recColliderDisableNode(Transform parent, List<Transform> disableNodeRec)
    {
        Collider col = parent.GetComponent<Collider>();
        if (col != null)
        {
            if (!col.enabled)
                disableNodeRec.Add(col.transform);
        }

        for (int a = 0; a < parent.childCount; a++)
        {
            _recColliderDisableNode(parent.GetChild(a), disableNodeRec);
        }
    }

    static void _setAllTree_Collider(Transform root, bool enable)
    {
        _setAllTree_Collider(root, enable, null);
    }

    static void _setAllTree_Collider(Transform root, bool enable, List<Transform> mColliderDisableNode)
    {
        Collider col = root.GetComponent<Collider>();
        if (col != null)
        {
            if (enable)
            {
                if (mColliderDisableNode.IndexOf(col.transform) < 0)
                    col.enabled = true;
            }
            else
                col.enabled = false;
        }


        for (int a = 0; a < root.childCount; a++)
        {
            _setAllTree_Collider(root.GetChild(a), enable, mColliderDisableNode);
        }
    }

    public static void _getObjByName(Transform parent, string name, ref Transform outObj)
    {
        if (outObj != null)
            return;

        if (parent.name == name)
            outObj = parent;

        for (int a = 0; a < parent.childCount; a++)
        {
            _getObjByName(parent.GetChild(a), name, ref outObj);
        }
    }

    //bool _isHeadCrack()
    //{
    //    return !HeadMesh.enabled;
    //}

    public void Respawn()
    {
        _setAllTree_Collider(Actor, true, mColliderDisableNode);

        HeadMesh.enabled = true;
        foreach (BodyCrackInfo info in mBodyCrackInfo)
        {
            info.CrackMeshPart.enabled = true;
            if (info.LeaveMeshParts != null)
            {
                foreach (SkinnedMeshRenderer renderer in info.LeaveMeshParts)
                {
                    renderer.enabled = true;
                }
            }
        }

        mBleedDataList.Clear();
        mAnimator.speed = 1;
        mFallDownTimeCountSec = null;
        mRagdollUtility.DisableRagdoll();
    }


  
}
