﻿using UnityEngine;
using System.Collections;
using System;

public class GunFireEffectCaster2 : ShootEffectCasterBase
{
    public GameObject ZombiePrefeb;
    public GameObject muzzleEffectPlace;
    public GameObject cartridgeEffectPlace;
    public GameObject muzzleEffect;
    public GameObject cartridgeEffect;
    public GameObject impactEffect;
    public Animation ShootAnimation;

    protected override void OnFire()
    {
        if (ShootAnimation != null)
        {
            ShootAnimation.Stop();            
            ShootAnimation.Play();
        }

        Ray ray = new Ray(muzzleEffectPlace.transform.position, muzzleEffectPlace.transform.forward);

        GameObject effect = Instantiate(muzzleEffect, muzzleEffectPlace.transform.position, muzzleEffectPlace.transform.rotation) as GameObject;
        effect.transform.parent = muzzleEffectPlace.transform;

        Instantiate(cartridgeEffect, cartridgeEffectPlace.transform.position, cartridgeEffectPlace.transform.rotation);
        //Instantiate(cartridgeEffect[cartridgeEffectSelected], cartridgeEffectPlace.transform.position, cartridgeEffectPlace.transform.rotation);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Transform zombie = null;
            MyHelpNode.FindParentTag(hit.collider.transform, ZombiePrefeb.tag, ref zombie);
            if (zombie == null)
            {
                Vector3 forward = Vector3.Cross(hit.normal, Vector3.right);
                Quaternion quat = Quaternion.LookRotation(forward, hit.normal);
                Instantiate(impactEffect, hit.point, quat);// hit.collider.gameObject.transform.rotation);

                //  Debug.Log("Hit ground");
            }
            else
            {
                Player_Portal player_Portal = zombie.GetComponentInChildren<Player_Portal>();
                if (player_Portal != null)
                    player_Portal.HittedZombie(hit, ray);

                // Debug.Log("Hit zombie");
            }
        }
    }

    protected override void OnStopFire()
    {
        
    }
}




