﻿using UnityEngine;
using System.Collections;
using System;

public class LongbowEffectCaster : ShootEffectCasterBase
{
    public Transform ShootOutPosition;
    public GameObject HideLocalArrow;
    public float HideLocalArrowTime;
    public GameObject ArrowPrefeb;
    public GameObject ZombiePrefeb;
    public Animator BowAnimator;

    void Start()
    {
        BowAnimator.speed = 0;
    }

    public void SetSpringPullRatio(float springPullRatio)
    {
        BowAnimator.Play("Pull", 0, springPullRatio);
        //Debug.Log("SetSpringPullRatio : " + springPullRatio);
    }

    public void SetArrowShootOut(float pullRatio, Vector3 direction)
    {
        BowAnimator.speed = 1;
        BowAnimator.SetBool("ShootOut", true);
        StartCoroutine(_shootOutArrow(pullRatio, direction));
    }

    public void SetArrowOnSpring()
    {
        BowAnimator.SetBool("ShootOut", false);
        HideLocalArrow.SetActive(true);
    }

    protected override void PlayGunFireSound()
    {
        //Not play at trigger
        //base.PlayGunFireSound();
    }

    protected override void OnFire()
    {
    }

    protected override void OnStopFire()
    {
    }

    IEnumerator _shootOutArrow(float pullRatio, Vector3 direction)
    {
        if (HideLocalArrow != null)
        {
            yield return new WaitForSeconds(HideLocalArrowTime);
            HideLocalArrow.SetActive(false);
        }

        GameObject arrow = Instantiate(ArrowPrefeb, ShootOutPosition.transform.position, Quaternion.LookRotation(direction)) as GameObject;
        arrow.SetActive(true);
        arrow.GetComponent<MyHelps.ArrowFlying>().SetArrowHitEvent(_arrowHitEvent, pullRatio);

        base.PlayGunFireSound();
    }

    void _arrowHitEvent(RaycastHit hit, Ray hitRay)
    {
        Transform zombie = null;
        MyHelpNode.FindParentTag(hit.collider.transform, ZombiePrefeb.tag, ref zombie);
        if (zombie == null)
        {
            Quaternion quat = Quaternion.LookRotation(hit.normal, Vector3.up);
            //Instantiate(impactEffect[impactEffectSelected], hit.point, quat);// hit.collider.gameObject.transform.rotation);

            //  Debug.Log("Hit ground");
        }
        else
        {
            Player_Portal player_Portal = zombie.GetComponentInChildren<Player_Portal>();
            if (player_Portal != null)
                player_Portal.HittedZombie(hit, hitRay);

            // Debug.Log("Hit zombie");
        }
    }


}
