﻿using UnityEngine;
using System.Collections;
using System;

public class CrossbowEffectCaster : ShootEffectCasterBase
{
    public Transform ShootOutPosition;
    public Animation ShootAnimation;
    public GameObject HideLocalArrow;
    public float HideLocalArrowTime;
    public GameObject ArrowPrefeb;
    public GameObject ZombiePrefeb;


    protected override void OnFire()
    {
        if (ShootAnimation != null)
        {
            ShootAnimation.Stop();
            ShootAnimation.GetComponent<Animation>()["Shot"].speed = 3;
            ShootAnimation.Play();
        }

        HideLocalArrow.GetComponent<Renderer>().enabled = true;
        StartCoroutine(_shootOutArrow());
    }

    IEnumerator _shootOutArrow()
    {
        yield return new WaitForSeconds(HideLocalArrowTime);

        HideLocalArrow.GetComponent<Renderer>().enabled = false;
        //Debug.Log("_shootOutArrow");

        GameObject arrow = Instantiate(ArrowPrefeb, ShootOutPosition.transform.position, ShootOutPosition.transform.rotation) as GameObject;
        arrow.SetActive(true);
        arrow.GetComponent<MyHelps.ArrowFlying>().SetArrowHitEvent(_arrowHitEvent);
    }
    
    void _arrowHitEvent(RaycastHit hit, Ray hitRay)
    {
        Transform zombie = null;
        MyHelpNode.FindParentTag(hit.collider.transform, ZombiePrefeb.tag, ref zombie);
        if (zombie == null)
        {
            Quaternion quat = Quaternion.LookRotation(hit.normal, Vector3.up);
            //Instantiate(impactEffect[impactEffectSelected], hit.point, quat);// hit.collider.gameObject.transform.rotation);

            //  Debug.Log("Hit ground");
        }
        else
        {
            Player_Portal player_Portal = zombie.GetComponentInChildren<Player_Portal>();
            if (player_Portal != null)
                player_Portal.HittedZombie(hit, hitRay);

            // Debug.Log("Hit zombie");
        }
    }

    protected override void OnStopFire()
    {
        
    }
}
