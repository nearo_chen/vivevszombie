﻿using UnityEngine;
using System.Collections;

public class GunFireMouseTest2 : MonoBehaviour
{
    public ShootEffectCasterBase EffectCaster;
    public bool UseMouseToFire = false;

    public bool UseMouseNotRotate = false;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!UseMouseToFire)
            return;

        if (!UseMouseNotRotate)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
                transform.LookAt(hit.point);
        }

        if (Input.GetMouseButton(0))
        {
            if (EffectCaster != null)
                EffectCaster.Fire(true);
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (EffectCaster != null)
                EffectCaster.Fire(false);
        }
    }

    public void Fire()
    {
        if (EffectCaster != null)
            EffectCaster.Fire(true);
    }

    public void NotFire()
    {
        if (EffectCaster != null)
            EffectCaster.Fire(false);
    }
}
