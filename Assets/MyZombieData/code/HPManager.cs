﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HPManager : MonoBehaviour
{
    public Transform Actor;
    float HP = 100;
    bool ActorFallingDown = false;

    public BodyCrackEffect mBodyCrackEffect;
    public ZombieSounds zombieSounds;

    class BodyHPData
    {
        public BodyHPData(Transform bone, float hp, float hurtRatio)
        { Bone = bone; BodyPartHP = hp; HurtRatio = hurtRatio; }
        public BodyHPData(Transform bone, float hp, float hurtRatio, bool crack_Need_HP0)
        { Bone = bone; BodyPartHP = hp; HurtRatio = hurtRatio; Crack_fallDownimmediately = crack_Need_HP0; }
        public Transform Bone;
        public float BodyPartHP;
        public float HurtRatio;
        public bool Crack_fallDownimmediately;
    }
    List<BodyHPData> mBodyHPData = new List<BodyHPData>();

    float smallHandGunPower = 10;

    // Use this for initialization
    void Start()
    {
        mBodyCrackEffect = GetComponentInChildren<BodyCrackEffect>();

        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.LHand, 100, 4));
        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.RHand, 100, 4));

        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.LForeArm, 100, 4));
        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.RForeArm, 100, 4));

        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.LUpperArm, 100, 3));
        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.RUpperArm, 100, 3));

        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.LChest, 100, 1));
        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.RChest, 100, 1));

        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.LWaist, 100, 2));
        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.RWaist, 100, 2));

        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.LThigh, 100, 3, true));
        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.RThigh, 100, 3, true));

        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.LCalf, 100, 3, true));
        mBodyHPData.Add(new BodyHPData(mBodyCrackEffect.RCalf, 100, 3, true));
    }

    
    // Update is called once per frame
    void Update()
    {
        
    }

   public void Respawn(Vector3 pos)
    {
        for (int a = 0; a < mBodyHPData.Count; a++)
            mBodyHPData[a].BodyPartHP = 100;
        HP = 100;
        ActorFallingDown = false;
        mBodyCrackEffect.Respawn();
        zombieSounds.InitState();
        Actor.position = pos;
    }

    //bool _checkBodyHitted(string hitName)
    //{
    //    //if (hitName == mBodyCrackEffect.Pelvis.name ||
    //    //   hitName == mBodyCrackEffect.UpperSpine.name
    //    //    )
    //    //    return false;

    //    Transform outObj = null;
    //    BodyCrackEffect._getObjByName(Actor, hitName, ref outObj);
    //    if (outObj == null)
    //        return false;
    //    return true;
    //}

    public BodyCrackEffect.BodyCrackInfo GetBodyCrackInfo(Transform bone)
    {
        return mBodyCrackEffect.GetBodyCrackInfo(bone);
    }

    
    //扣血的機制搬到ｍａｉｎ，然後哪個部位要爆炸，再call BodyCrackEffect的function
    public void SetHitName(string hitName, Vector3 hitPoint, Transform hitBone, Ray hitRay)
    {
        //if (!_checkBodyHitted(hitName))
        //    return;
        
        bool bodyCrackAddforce = false;
        
        if (hitName == mBodyCrackEffect.Head.name)
        {
            HP -= smallHandGunPower * 3;
            if (HP <= 0)
            {
                mBodyCrackEffect.Crack_Head();
                bodyCrackAddforce = true;
            }
            else
                mBodyCrackEffect._body_hit(hitPoint, hitBone, false);
        }
        else
        {
            BodyHPData bodyHPData = null;
            foreach (BodyHPData data in mBodyHPData)
            {
                if (data.Bone.name == hitName)
                {
                    bodyHPData = data;
                    break;
                }
            }

            if (bodyHPData != null)
            {
                HP -= smallHandGunPower;
                bodyHPData.BodyPartHP -= smallHandGunPower * bodyHPData.HurtRatio;
                if (bodyHPData.Crack_fallDownimmediately && HP <= 0 && bodyHPData.BodyPartHP <= 0)
                {
                    mBodyCrackEffect.Crack_BodyPart(bodyHPData.Bone);
                   
                    bodyCrackAddforce = true;
                }
                else if (bodyHPData.BodyPartHP <= 0 && !bodyHPData.Crack_fallDownimmediately)
                {
                    mBodyCrackEffect.Crack_BodyPart(bodyHPData.Bone);
                    if (HP <= 0)
                        bodyCrackAddforce = true;
                }
                else
                    mBodyCrackEffect.Body_hit(hitPoint, hitBone);
            }
            else
            {
                HP -= smallHandGunPower;
                mBodyCrackEffect.Body_hit(hitPoint, hitBone);
            }
        }

        if (HP <= 0 && !ActorFallingDown)
        {
            ActorFallingDown = true;
            float falldownTime = 0;
            if (!bodyCrackAddforce)
                falldownTime = Random.Range(1.0f, 3.0f);
            mBodyCrackEffect.ActorFallDown(falldownTime);

            //Debug.Log("fallDownimmediately: " + fallDownimmediately + " , bodyCrackAddforce: "+ bodyCrackAddforce);
            if (bodyCrackAddforce)
                mBodyCrackEffect.AddForce_RigidBody(hitBone, hitRay);
        }

        if (bodyCrackAddforce)
            zombieSounds.dead();
        else
            zombieSounds.hurt();
    }

    public float GetHP()
    { return HP; }

    void OnGUI()
    {
        string text = string.Empty;
        foreach (BodyHPData data in mBodyHPData)
        {
            text += "Name: " + data.Bone.name + " , HP: " + data.BodyPartHP + " , ratio: " + data.HurtRatio + "\n";
        }
        text += "HP: " + HP;

        GUILayout.Label(text);


    }
}
