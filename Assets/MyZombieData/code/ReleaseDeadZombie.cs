﻿using UnityEngine;
using System.Collections;

public class ReleaseDeadZombie : MonoBehaviour
{
    public HPManager mHPManager;
    public Transform DebugRespawnPos;
    public Transform Zombie;

    float? mDeadFadeOutTimeCount;
    float? mDeadFadeOutTimeCount2;

    // Use this for initialization
    void Start() {

    }

    void Respawn()
    {
        mDeadFadeOutTimeCount2 = mDeadFadeOutTimeCount = null;
        mHPManager.Respawn(DebugRespawnPos.position);
    }

	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyUp(KeyCode.S))
        {
            Respawn();
        }

        float zomBieHP = mHPManager.GetHP();
        if (zomBieHP <= 0 && mDeadFadeOutTimeCount == null)
            mDeadFadeOutTimeCount = 5;

        //check if zombie dead
        if (zomBieHP <= 0 && mDeadFadeOutTimeCount.HasValue && mDeadFadeOutTimeCount2 == null)
        {
            mDeadFadeOutTimeCount -= Time.deltaTime;
            if (mDeadFadeOutTimeCount <= 0)
            {
                MyHelpNode.CloseColliderRigidBody(Zombie, 20);
                mDeadFadeOutTimeCount2 = 2;
                Zombie.GetComponent<RootMotion.FinalIK.RagdollUtility>().enabled = false;
                Zombie.GetComponentInChildren<CloneHand>().CloseCollider();
            }
        }
        else if (zomBieHP <= 0 && mDeadFadeOutTimeCount2.HasValue)
        {
            mDeadFadeOutTimeCount2 -= Time.deltaTime;
            if (mDeadFadeOutTimeCount2 <= 0)
                Destroy(Zombie.gameObject);
        }
    }

   
}
