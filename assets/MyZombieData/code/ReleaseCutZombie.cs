﻿using UnityEngine;
using System.Collections;

public class ReleaseCutZombie : MonoBehaviour {

    public float ReleaseTime = 3;
    float mTimeCount;

    int releaseStage = 0;

	// Use this for initialization
	void Start () {
        mTimeCount = ReleaseTime;
    }
	
	// Update is called once per frame
	void Update () {
        mTimeCount -= Time.deltaTime;
        if(mTimeCount < 0 && releaseStage==0)
        {
            MyHelpNode.CloseColliderRigidBody(transform, 20);
            releaseStage = 1;
            mTimeCount = 2;
        }
        else if(mTimeCount < 0 && releaseStage == 1)
        {
            Destroy(this.gameObject);
        }
    }
}
