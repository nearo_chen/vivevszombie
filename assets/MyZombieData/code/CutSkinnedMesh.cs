﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CutSkinnedMesh : MonoBehaviour
{
    public Transform TestZombieSkinning;
  
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Z))
        {
            Debug.Log("Skinned2Mesh");
            TestZombieSkinning.gameObject.SetActive(true);
            GameObject obj =SetCutData(TestZombieSkinning);
            obj.tag = "IsZombie";
        }
    }

    public static GameObject SetCutData(Transform zombie)
    {
        SkinnedMeshRenderer IsSkinned = zombie.GetComponentInChildren<SkinnedMeshRenderer>();
        if (IsSkinned == null)
        {
            //Debug.Log("SetCutData : not skinned mesh");
            return zombie.gameObject;
        }

        GameObject cloneObj = Skinned2Mesh(zombie);        
          
        _addShatterTool(cloneObj);

        return cloneObj;
    }

    static void _addSkinnedMesh(Transform currentNode, List<SkinnedMeshRenderer> skinmeshList)
    {
        SkinnedMeshRenderer skin = currentNode.GetComponent<SkinnedMeshRenderer>();
        if (skin != null)
            skinmeshList.Add(skin);

        for(int a=0; a< currentNode.childCount; a++)
        {
            _addSkinnedMesh(currentNode.GetChild(a), skinmeshList);
        }
    }

    static SkinnedMeshRenderer[]  _getAllSkinnedMeshes(Transform zombie)
    {
        List<SkinnedMeshRenderer> skinmeshList = new List<SkinnedMeshRenderer>();
        _addSkinnedMesh(zombie.transform, skinmeshList);

        return skinmeshList.ToArray();
    }

    public static GameObject Skinned2Mesh(Transform zombie)
    {
        SkinnedMeshRenderer[] skinnedMeshes = zombie.GetComponentsInChildren<SkinnedMeshRenderer>();
        List<CombineInstance> combineList = new List<CombineInstance>();

        GameObject parent = new GameObject();
        parent.name = zombie.name + "_cut" + Random.Range(0,1000);

        MeshRenderer meshRenderer = parent.AddComponent<MeshRenderer>();
        MeshFilter mshFilter = parent.AddComponent<MeshFilter>();

        bool isInitMaterial = false;
        for(int a=0; a< skinnedMeshes.Length; a++)
        {
            SkinnedMeshRenderer skinRenderer = skinnedMeshes[a];
           
            if (skinRenderer.enabled == false)
                continue;
            if (!isInitMaterial)
            {
                isInitMaterial = true;
                meshRenderer.material = Instantiate(skinRenderer.material);
            }

            CombineInstance combine = new CombineInstance();
            Mesh mesh = new Mesh();
            skinRenderer.BakeMesh(mesh);

            combine.mesh = mesh;
            combine.transform = skinRenderer.transform.localToWorldMatrix;
            combineList.Add(combine);
        }
        mshFilter.mesh.CombineMeshes(combineList.ToArray());
        //parent.SetActive(true);

        //add collider
        MeshCollider collider = parent.AddComponent<MeshCollider>();
        collider.convex = true;
        collider.sharedMesh = parent.GetComponent<MeshFilter>().mesh;

        //add Rigidbody
        parent.AddComponent<Rigidbody>();


        //GameObject grandParent = new GameObject();
        //parent.transform.parent = grandParent.transform;
        //ShatterTool stool=grandParent.AddComponent<ShatterTool>();
        //stool.InternalHullType = HullType.FastHull;
        //stool.SendPostSplitMessage = true;
        //stool.SendPreSplitMessage = true;
        //stool.FillCut = true;
        //HierarchyHandler hierarchy = grandParent.AddComponent<HierarchyHandler>();
        //hierarchy.attachPieceToParent = true;

        return parent;
    }


    static void _addShatterTool(GameObject zombieMesh)
    {
        MeshRenderer renderer = zombieMesh.GetComponent<MeshRenderer>();
        MeshCollider collider = zombieMesh.GetComponent<MeshCollider>();
        if (renderer != null && collider != null)
        {
            ShatterTool stool = zombieMesh.gameObject.AddComponent<ShatterTool>();
            stool.InternalHullType = HullType.FastHull;
            stool.SendPostSplitMessage = false;
            stool.SendPreSplitMessage = false;
            stool.FillCut = true;

            zombieMesh.gameObject.AddComponent<WorldUvMapper>();

          //  HierarchyHandler hierarchy = zombieMesh.gameObject.AddComponent<HierarchyHandler>();
          //  hierarchy.attachPieceToParent = true;
           // hierarchy.       
        }
        else
        {
            Debug.LogError("_addShatterTool :"+ zombieMesh.name + "!(renderer != null && collider != null)");
        }
    }
}
