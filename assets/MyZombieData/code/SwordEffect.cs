﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MyHelps;

public class SwordEffect : MonoBehaviour
{


    public float SwordLength;
    // public Transform Blade;
    public GameObject ZombiePrefeb;
    public int raycastCount;
    public ShatterScheduler scheduler = null;

    CutPositionData? BeginVec;
    CutPositionData? EndVec;
    Transform mHittedZombie;

    public GameObject BloodFXPrefeb;

    struct CutPositionData
    {
        public Transform cutBone;
        public Vector3 offset;
    }

    // Use this for initialization
    void Start()
    {


        mHittedZombie = null;
    }

    int ProcessCut = int.MaxValue;
    //GameObject mDebugCutObject;
    Plane? mDebugPlane;
    Plane SplitPlane;
    Vector3 CutDirection, CutBegin, CutEnd;
    GameObject SplitObj;

    // Update is called once per frame
    void Update()
    {
        foreach (CutParticleData data in mCutParticleDataList)
        {
            if (data.particleObj == null || data.bodyCutObject == null)//detect be destroy
            {
                if (data.particleObj != null)
                    Destroy(data.particleObj);
                mCutParticleDataList.Remove(data);
                break;
            }

            Matrix4x4 matRb = Matrix4x4.TRS(data.Rb.position, data.Rb.rotation, Vector3.one);
            Matrix4x4 world = matRb * data.localMat;
            //debug_Begin.position = world.GetColumn(3);
            // Debug.Log("debug_Begin.position : " + debug_Begin.position);

            MyHelpNode.SetWorldPosition(data.particleObj.transform, world.GetColumn(3));
            MyHelpNode.SetWorldRotation(data.particleObj.transform, MyHelp.QuaternionFromMatrix(ref world));
        }

        if (mDebugPlane.HasValue)
        {
            Vector3 basePos = transform.position;
            basePos += Vector3.one * 0.01f;
            MyHelp.DrawPlane(mDebugPlane.Value, basePos);
        }

        Vector3 swordDirection = transform.TransformVector(Vector3.forward);
        Debug.DrawLine(transform.position, transform.position + swordDirection * SwordLength,
            Color.white);

        bool isHitting = false;
        RaycastHit hit;
        Ray ray = new Ray(transform.position, swordDirection);
        if (Physics.Raycast(ray, out hit))
        {
            Transform zombie = null;
            MyHelpNode.FindParentTag(hit.collider.transform, ZombiePrefeb.tag, ref zombie);
            if (zombie != null)
            {
                //check in sword range
                if (hit.distance < SwordLength)
                {
                    if (mHittedZombie == null)
                    {
                        mHittedZombie = zombie;
                        CutPositionData data;
                        data.cutBone = hit.collider.transform;
                        data.offset = hit.collider.transform.InverseTransformPoint(hit.point);
                        BeginVec = data;
                        isHitting = true;
                        //  Debug.Log("First Cut Zombie");
                    }
                    else if (mHittedZombie != null && zombie == mHittedZombie)
                    {
                        CutPositionData data;
                        data.cutBone = hit.collider.transform;
                        data.offset = hit.collider.transform.InverseTransformPoint(hit.point);
                        EndVec = data;
                        isHitting = true;
                        //       Debug.Log("End Cut Zombie : " + zombie.name + " , mHittedZombie:" + mHittedZombie.name);
                        mDebugPlane = null;
                    }
                    else
                    {
                        mHittedZombie = null;
                    }
                }
            }
        }

        if (!isHitting && (BeginVec == null || EndVec == null))
        {
            //  Debug.Log("(!isHitting && (BeginVec == null || EndVec == null))");
            BeginVec = null;
            EndVec = null;
            mHittedZombie = null;
        }
        else if (!isHitting && mHittedZombie != null)
        {
            //     Debug.Log("Cut Zombie finished..............");
            CutBegin = BeginVec.Value.cutBone.TransformPoint(BeginVec.Value.offset);
            CutEnd = EndVec.Value.cutBone.TransformPoint(EndVec.Value.offset);
            CutDirection = (CutBegin - CutEnd).normalized;
            // debug_Begin.transform.position = beginPos;
            // debug_End.transform.position = endPos;

            //Plane splitPlane = new Plane(Vector3.Normalize(Vector3.Cross(endPos-beginPos, swordDirection)), Vector3.zero);
            //splitPlane = MyHelp.TransformPlane(Matrix4x4.TRS(endPos,Quaternion.identity,Vector3.one), splitPlane);
            //Plane splitPlane = new Plane(Vector3.Cross(beginPos.normalized, endPos.normalized), endPos);
            SplitPlane = new Plane(
                Vector3.Cross((CutBegin - transform.position).normalized, ray.direction).normalized, ray.origin);

            Player_Portal player = mHittedZombie.GetComponentInChildren<Player_Portal>();
            if (player != null)
            {
                player.Splat_Blood_OnScreen((mHittedZombie.position - player.Player.position).sqrMagnitude);
                //Debug.Log("Splat_Blood_OnScreen");
            }

            // mDebugPlane = SplitPlane;

            SplitObj = CutSkinnedMesh.SetCutData(mHittedZombie);
            SplitObj.GetComponent<ShatterTool>().SwordObject = this.gameObject;
            SplitObj.tag = ZombiePrefeb.tag;
            // Debug.Log("CutSkinnedMesh.SetCutData(mHittedZombie);..............");
            //mDebugCutObject = SplitObj;

            //scheduler.AddTask(new SplitTask(splitObj, new Plane[] { splitPlane }));
            ProcessCut = 0;




            BeginVec = null;
            EndVec = null;
            mHittedZombie.gameObject.SetActive(false);
            mHittedZombie = null;
        }

        ProcessCut++;
        if (ProcessCut == 2)//process cut at next frame
        {
            //Debug.Log("ProcessCut..............");

            scheduler.AddTask(new SplitTask(SplitObj, new Plane[] { SplitPlane }));

            //BloodFX_Splash04------------------------------------------------------
            //obj = Instantiate(BloodFX_HeadSplash) as GameObject;
            //obj.transform.parent = Head;
            //_setAllTree_LocalPosition(obj.transform, Vector3.zero);
        }
    }

    struct CutParticleData
    {
        public GameObject bodyCutObject;
        public GameObject particleObj;

        //  public Vector3 offsetPosition;
        //  public Quaternion offsetRotation;
        public Matrix4x4 localMat;
        public Rigidbody Rb;
    }
    List<CutParticleData> mCutParticleDataList = new List<CutParticleData>();

    GameObject[] mNewCutObjects;
    public void RecieveSplitNewGameObjects(GameObject[] newGameObjects)
    {
        mNewCutObjects = newGameObjects;
        //string text = "RecieveSplitNewGameObjects :　" + newGameObjects.Length;
        //foreach (GameObject obj in newGameObjects)
        //{
        //    text += " , " + obj.name;
        //}
        //Debug.Log(text);

        setSplitNewGameObject(mNewCutObjects[0], SplitPlane, CutDirection, 1.0f, BloodFXPrefeb,
                                   CutBegin, mCutParticleDataList);
        setSplitNewGameObject(mNewCutObjects[0], SplitPlane, CutDirection, 1.0f, BloodFXPrefeb,
                                   CutEnd, mCutParticleDataList);

        if (mNewCutObjects.Length >= 2)
        {
            setSplitNewGameObject(mNewCutObjects[1], SplitPlane, CutDirection, -1.0f, BloodFXPrefeb,
                                    CutBegin, mCutParticleDataList);
            setSplitNewGameObject(mNewCutObjects[1], SplitPlane, CutDirection, -1.0f, BloodFXPrefeb,
                                    CutEnd, mCutParticleDataList);
        }
    }

    static void setSplitNewGameObject(GameObject obj, Plane splitPlane, Vector3 cutDirection, float normalWay,
        GameObject particlePrefeb,
        Vector3 splashPosition, List<CutParticleData> list)
    {
        Vector3 normal = splitPlane.normal;
        Vector3 dir = cutDirection + normal * 5.0f * normalWay;
        dir.Normalize();
        obj.AddComponent<ReleaseCutZombie>();
        Rigidbody Rb = obj.GetComponent<Rigidbody>();
        MeshCollider Mc = obj.GetComponent<MeshCollider>();
        //  Debug.Log("Addforce[0] : " + Mc.bounds.size.magnitude);
        Rb.AddForce(dir * Mc.bounds.size.magnitude * 10);

        CutParticleData data;
        data.bodyCutObject = obj;
        data.particleObj = Instantiate(particlePrefeb) as GameObject;

        Matrix4x4 RbMat = Matrix4x4.TRS(Rb.position, Rb.rotation, Vector3.one);
        Matrix4x4 particleMat = Matrix4x4.TRS(splashPosition, Quaternion.LookRotation(normal), Vector3.one);
        data.localMat = Matrix4x4.Inverse(RbMat) * particleMat;
        data.Rb = Rb;
        list.Add(data);


    }

    public Transform debug_Begin, debug_End;
}
